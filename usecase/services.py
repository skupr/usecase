import abc
from dataclasses import dataclass
from typing import Dict, List, Tuple
from logging import getLogger

from pydantic import BaseModel, ValidationError


logger = getLogger(__name__)


class BaseService(abc.ABC):
    class ValidationError(Exception):
        def __init__(self, *args, errors: Dict[str, List[Tuple[str, str]]]):
            self.errors = errors
            super().__init__(*args)

    @abc.abstractmethod
    def process(self):
        pass

    @classmethod
    @abc.abstractmethod
    def run(cls, *args, **kwargs):
        pass


@dataclass
class PlaneService(BaseService):
    """
    @dataclass should be used for descendant classes to pass arguments.
    """
    @abc.abstractmethod
    def process(self):
        pass

    @classmethod
    def run(cls, *args, **kwargs):
        service = cls(*args, **kwargs)
        return service.process()


class ModelService(BaseService, BaseModel):
    _messages = {}

    @abc.abstractmethod
    def process(self):
        pass

    @classmethod
    def run(cls, *args, **kwargs):
        try:
            service = cls(**kwargs)
        except ValidationError as e:
            raise cls.ValidationError(errors=cls._prepare_errors(e))

        return service.process()

    @classmethod
    def _prepare_errors(cls, e: ValidationError):
        """
        Transform pydantic errors to fit ServiceResponse signature.
        """
        prepared_errors = {}
        for err in e.errors():
            field_name = err["loc"][0]
            err_list = prepared_errors.get(field_name, [])
            err_type = err["type"]

            # Translations.
            msg = cls._messages.get(err_type, err["msg"])
            ctx = err.get('ctx', {})
            msg = msg.format(**ctx)
            logger.debug(f'err_type: {err_type} | context: {ctx}')

            err_list.append((err_type, msg))
            prepared_errors[field_name] = err_list
        return prepared_errors


class ServiceMeta(abc.ABCMeta):
    """
    This metaclass allows to encapsulate all small services by one object.
    """
    def __getattribute__(self, name):
        attr = super().__getattribute__(name)
        if hasattr(attr, 'run'):
            return attr.run

        return attr


class ServiceWrapper(metaclass=ServiceMeta):
    """
    The only one publicly accessible wrapper around a group of services.
    Usage:
        class Service(ServiceWrapper):
            create = CreateOrderService
            get = GetOrderInfoService

        try:
            resp = Service.create(**kwargs)
        except Service.ValidationError as e:
            raise SomeValidationError(e.errors)
    """
    ValidationError = BaseService.ValidationError
